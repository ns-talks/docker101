const express = require('express');
const data = require('./data.json');
const app = express();
const fs = require('fs');

// Ruta de ejemplo
app.get('/hola_mundo', (req, res) => {
    res.send('¡Hola, mundo!');
});

app.get('/data', async (req, res) => {
    const file = await fs.readFileSync('./data.json')
    console.log(file);
    res.send(data);
});

// Puerto en el que se ejecutará el servidor
const port = 3000;

// Inicia el servidor
app.listen(port, () => {
    console.log(`Servidor escuchando en el puerto ${port}`);
});
