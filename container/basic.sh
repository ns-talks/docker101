# Ejecuta una imagen. En caso que la imagen no se encuentre dentro de nuestro docker el procede a descargar la imagen en Docker hub
docker run mongo

# Ejecuta la imagen de mongo exponiendo su puerto hacia el host, es decir nosotros
docker run -p 27017:27017 mongo

# Ejecuta una imagen en modo deattach. Es decir la ejecución no vive en nuestra terminal
docker run -d -p 27017:27017 mongo

# Detiene la ejecución de un contenedor
docker stop container-id

# Reanuda la ejecución de un contenedor
docker rm container-id

# Elimina todos los contenedores de acuerdo a un patron en especifico
docker ps -a | grep "pattern" | awk '{print $1}' | xargs docker rm

# Reanuda la ejecución de un contenedor
docker start container-id

# Muestra todos los contenedores en ejecución
docker ps

# Muestra todos los contenedores
docker ps -a
