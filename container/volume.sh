# Crea un volume dentro de docker
docker create volume mongo-db;

# Muestra todos los volumenes creados en docker
docker volumen ls

# Elimina un volumen dentro de Docker
docker volume rm mongo-db

# Ejemplo de como se utiliza un volumen con un contenedor en mongo
docker run -p 27017:27017 -v mongo-db:/data/db mongo
