#!/bin/bash

#Limpia todas las imagenes sin usar dentro de Docker
docker image prune;

# Muestra todas las imagenes que estan en uso
docker image ls;

# Muestra todas las imagenes
docker image ls -a;

# Elimina una imagen dentro de docker con un tag en específico
docker image rm mongo:latest

# Es totalmente valido ocupar comandos de linux para hacer tu vida mucho mas sencilla
docker image ls | grep mongo

# Desde el Docker Hub (Este es el Docker Registry Publico de Docker) obtiene la imagen deseada
docker pull mongo:latest
