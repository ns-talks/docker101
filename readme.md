# Docker 101


# Que es un Docker Hub

Docker Hub es un repositorio público de imágenes de contenedores, donde muchas empresas e individuos pueden publicar imágenes de soluciones prediseñadas. Estas soluciones van desde WordPress hasta Magento y muchas otras aplicaciones.

Existe la posibilidad de tener un repositorio privado? Claro todo es posible con el Docker Registry


# Docker Registry

Docker Registry es una forma habitual de almacenar y distribuir imágenes de Docker. Se trata de un repositorio basado en código abierto con la cesión de licencia de Apache.

Docker Hub es un Docker Registry alojado y administrado por Docker. También se pueden hacer uso plataformas como AWS, Azure Container Registry o Oracle Container Registry para poder subir tus imágenes y poder conservarlas privadas.

[Link Configuración Registry](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-18-04-es)

Comandos para utilizar un registry



* docker login {docker-registry-url}
* docker pull image-name


### 


# Que es Docker

Docker es una plataforma de software que permite crear, probar e implementar aplicaciones de forma rápida y estándar. Este empaqueta el software en unidades estandarizadas llamadas contenedores que incluyen todo lo necesario para que el software se ejecute, incluidas bibliotecas, herramientas de sistema, código y tiempo de ejecución. Con Docker, puede implementar y ajustar la escala de aplicaciones rápidamente en cualquier entorno con la certeza de saber que su código se ejecutará.


# Como funciona Docker

La tecnología Docker utiliza el kernel de Linux y sus funciones, como los grupos de control y los espacios de nombre, para dividir los procesos y ejecutarlos de manera independiente.


# Diferencia entre Docker y VM

Docker utiliza el aislamiento de recursos en el kernel del sistema operativo para ejecutar varios contenedores en el mismo sistema operativo. Esto es diferente a las máquinas virtuales (VM), que encapsulan un sistema operativo completo con código ejecutable sobre una capa abstracta de recursos de hardware físico.


![alt_text](https://guias.donweb.com/wp-content/uploads/2022/01/docker-vs-virtual-machine.png "Docker vs VM")



### 


# Qué son las imágenes en Docker

Una imagen de Docker es una plantilla de solo lectura que define su contenedor. La imagen contiene el código que se ejecutará, incluida cualquier definición para cualquier biblioteca o dependencia que el código necesite.


# Qué son los contenedores en Docker

Un contenedor de Docker es un conocido contenedor ejecutable, independiente, ligero que integra todo lo necesario para ejecutar una aplicación, incluidas bibliotecas, herramientas del sistema, código y tiempo de ejecución


# Porque usar Docker


#### Desarrollo local simplificado:

Docker facilita la configuración de entornos de desarrollo locales reproducibles.


#### Pruebas y Depuración

Puedes crear rápidamente instancias aisladas de la aplicación en diferentes entornos para realizar pruebas unitarias.


#### Implementación rápida y rollback sencillo

Si algo sale mal durante la implementación, es posible realizar un rollback fácilmente volviendo a la versión anterior de la imagen del contenedor.

En resumen, Docker es útil en diversos escenarios, desde el desarrollo local y las pruebas hasta la implementación y el despliegue de aplicaciones en producción. Proporciona una forma eficiente, portátil y aislada de ejecutar aplicaciones, simplifica la gestión de versiones y colaboración, y ofrece herramientas para la orquestación y el despliegue automatizado.


## Bibliografía

[https://www.oracle.com/co/cloud/cloud-native/container-registry/what-is-docker/](https://www.oracle.com/co/cloud/cloud-native/container-registry/what-is-docker/)

[https://www.redhat.com/es/topics/containers/what-is-docker](https://www.redhat.com/es/topics/containers/what-is-docker)

[https://www.oracle.com/co/cloud/cloud-native/container-registry/what-is-docker/](https://www.oracle.com/co/cloud/cloud-native/container-registry/what-is-docker/)

[https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-18-04-es](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-18-04-es)

[https://learn.microsoft.com/es-es/virtualization/windowscontainers/manage-docker/manage-windows-dockerfile](https://learn.microsoft.com/es-es/virtualization/windowscontainers/manage-docker/manage-windows-dockerfile)
